#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#include <libubox/blobmsg.h>
#include <libubox/blobmsg_json.h>
#include <libubox/uloop.h>
#include <libubus.h>

#include "bbfd.h"
#include "bbf.h"
#include "strncpyt.h"

static struct ubus_context *ctx;
static struct blob_buf bb;

#ifndef MAXNAMLEN
#define MAXNAMLEN 512
#endif

#define OPERATE_RESPONSE_MAXLEN 32768
#define OBJ_NAME		"bbfd"
#define OBJ_NAME_RAW		"bbfd.raw"

#define err_ubus(fmt, ubus_rc, args...) \
	err(fmt ": %s\n", ##args, ubus_strerror(ubus_rc))

enum {
	DM_GET_PATHS,
	DM_GET_PROTO,
	DM_GET_NXT_LVL,
	__DM_GET_MAX
};

enum {
	DM_SETS_PATHS,
	DM_SETS_PROTO,
	__DM_SETS_MAX
};

enum {
	DM_SETS_A_NOTIF_PATH,
	DM_SETS_A_NOTIF_VALUE,
	DM_SETS_A_NOTIF_CHANGE,
	__DM_SETS_A_NOTIF_MAX
};

enum {
	DM_ADD_PATH,
	DM_ADD_PROTO,
	DM_ADD_PARAMETER_KEY,
	__DM_ADD_MAX
};

enum {
	DM_SET_PATH,
	DM_SET_VALUE,
	DM_SET_PROTO,
	DM_SET_PARAMETER_KEY,
	__DM_SET_MAX,
};

enum {
	DM_OPERATE_PATH,
	DM_OPERATE_PARAMS,
	DM_OPERATE_PROTO,
	__DM_OPERATE_MAX,
};

static const struct blobmsg_policy dm_get_safe_policy[__DM_GET_MAX] = {
	[DM_GET_PATHS] = { .name = "paths", .type = BLOBMSG_TYPE_ARRAY },
	[DM_GET_PROTO] = { .name = "proto", .type = BLOBMSG_TYPE_STRING },
	[DM_GET_NXT_LVL] = { .name = "next-level", .type = BLOBMSG_TYPE_INT8 },
};

static const struct blobmsg_policy dm_sets_policy[__DM_SETS_MAX] = {
	[DM_SETS_PATHS] = { .name = "paths", .type = BLOBMSG_TYPE_ARRAY },
	[DM_SETS_PROTO] = { .name = "proto", .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy dm_sets_attrib_policy[__DM_SETS_A_NOTIF_MAX] = {
	[DM_SETS_A_NOTIF_PATH] = { .name = "path", .type = BLOBMSG_TYPE_STRING },
	[DM_SETS_A_NOTIF_VALUE] = { .name = "notify-type", .type = BLOBMSG_TYPE_STRING },
	[DM_SETS_A_NOTIF_CHANGE] = { .name = "notify", .type = BLOBMSG_TYPE_STRING },
};

static const struct blobmsg_policy dm_add_policy[__DM_ADD_MAX] = {
	[DM_ADD_PATH] = { .name = "path", .type = BLOBMSG_TYPE_STRING },
	[DM_ADD_PROTO] = { .name = "proto", .type = BLOBMSG_TYPE_STRING },
	[DM_ADD_PARAMETER_KEY] = { .name = "key", .type = BLOBMSG_TYPE_STRING }
};

static const struct blobmsg_policy dm_set_policy[__DM_SET_MAX] = {
	[DM_SET_PATH] = { .name = "path", .type = BLOBMSG_TYPE_STRING },
	[DM_SET_VALUE] = { .name = "value", .type = BLOBMSG_TYPE_STRING },
	[DM_SET_PROTO] = { .name = "proto", .type = BLOBMSG_TYPE_STRING },
	[DM_SET_PARAMETER_KEY] = { .name = "key", .type = BLOBMSG_TYPE_STRING }
};

static const struct blobmsg_policy dm_operate_policy[__DM_OPERATE_MAX] = {
	[DM_OPERATE_PATH] = { .name = "path", .type = BLOBMSG_TYPE_STRING },
	[DM_OPERATE_PROTO] = { .name = "proto", .type = BLOBMSG_TYPE_STRING },
	[DM_OPERATE_PARAMS] = { .name = "params", .type = BLOBMSG_TYPE_TABLE }
};

struct bbfd_async_req {
	struct ubus_request_data req;
	struct uloop_process process;
	int data_fd;
};


static int get_safe(struct ubus_context *ctx,
			struct ubus_object *obj,
			struct ubus_request_data *req,
			struct blob_attr *msg,
			int bbf_cmd)
{
	struct blob_attr *tb[__DM_GET_MAX];
	struct blob_attr *paths;
	struct blob_attr *path;
	void *a;
	char *nxt_lvl;
	size_t rem;
	const int raw = !strcmp(obj->name, OBJ_NAME_RAW);

	blobmsg_parse(dm_get_safe_policy, __DM_GET_MAX,
			tb, blob_data(msg), blob_len(msg));

	paths = tb[DM_GET_PATHS];
	if (paths == NULL)
		return UBUS_STATUS_INVALID_ARGUMENT;

	if (tb[DM_GET_NXT_LVL]) {
		if (blobmsg_get_u8(tb[DM_GET_NXT_LVL]))
			nxt_lvl = "1";
		else
			nxt_lvl = "0";
	} else {
		/* Set next-level = false, as default on CMD_GET_NAME
 		 */
		if (bbf_cmd == CMD_GET_NAME)
			nxt_lvl = "0";
		else
			nxt_lvl = NULL;
	}

	bbf_set_data_type(tb[DM_GET_PROTO]);

	blob_buf_init(&bb, 0);

	if (raw)
		a = blobmsg_open_array(&bb, "parameters");

	blobmsg_for_each_attr(path, paths, rem) {
		char *path_str = blobmsg_get_string(path);

		if (raw)
			bbf_get_raw(bbf_cmd, path_str, &bb, nxt_lvl);
		else
			bbf_get_blob(bbf_cmd, path_str, &bb, nxt_lvl);
	}

	if (raw)
		blobmsg_close_array(&bb, a);

	ubus_send_reply(ctx, req, bb.head);
	return 0;
}

static int bbfd_get_attributes(struct ubus_context *ctx,
			struct ubus_object *obj,
			struct ubus_request_data *req,
			__unused const char *method,
			struct blob_attr *msg)
{
	return get_safe(ctx, obj, req, msg, CMD_GET_NOTIFICATION);
}

static int bbfd_get_values(struct ubus_context *ctx,
			struct ubus_object *obj,
			struct ubus_request_data *req,
			__unused const char *method,
			struct blob_attr *msg)
{
	return get_safe(ctx, obj, req, msg, CMD_GET_VALUE);
}

static int bbfd_get_names(struct ubus_context *ctx,
			struct ubus_object *obj,
			struct ubus_request_data *req,
			__unused const char *method,
			struct blob_attr *msg)
{
	return get_safe(ctx, obj, req, msg, CMD_GET_NAME);
}

static int bbfd_get_instances(struct ubus_context *ctx,
			struct ubus_object *obj,
			struct ubus_request_data *req,
			__unused const char *method,
			struct blob_attr *msg)
{
	return get_safe(ctx, obj, req, msg, CMD_GET_INSTANCES);
}

static int get_safe_noarg(struct ubus_context *ctx,
			struct ubus_request_data *req,
			int cmd)
{
	void *a;

	blob_buf_init(&bb, 0);
	a = blobmsg_open_array(&bb, "parameters");
	bbf_get_raw_usp_noarg(&bb, cmd);
	blobmsg_close_array(&bb, a);

	ubus_send_reply(ctx, req, bb.head);
	return 0;
}

static int bbfd_get_schema(struct ubus_context *ctx,
			__unused struct ubus_object *obj,
			struct ubus_request_data *req,
			__unused const char *method,
			__unused struct blob_attr *msg)
{
	return get_safe_noarg(ctx, req, CMD_GET_SCHEMA);
}

static int bbfd_get_operates(struct ubus_context *ctx,
			__unused struct ubus_object *obj,
			struct ubus_request_data *req,
			__unused const char *method,
			__unused struct blob_attr *msg)
{
	return get_safe_noarg(ctx, req, CMD_USP_LIST_OPERATE);
}


static int usp_set_attributes(struct ubus_context *ctx,
			__unused struct ubus_object *obj,
			struct ubus_request_data *req,
			__unused const char *method,
			struct blob_attr *msg)
{
	struct blob_attr *tb[__DM_SETS_MAX];
	struct blob_attr *paths;
	struct blob_attr *cur;
	void *a;
	size_t rem;

	blobmsg_parse(dm_sets_policy, __DM_SETS_MAX,
			tb, blob_data(msg), blob_len(msg));

	paths = tb[DM_SETS_PATHS];
	if (paths == NULL)
		return UBUS_STATUS_INVALID_ARGUMENT;

	bbf_set_data_type(tb[DM_SETS_PROTO]);

	blob_buf_init(&bb, 0);
	a = blobmsg_open_array(&bb, "parameters");

	blobmsg_for_each_attr(cur, paths, rem) {
		struct blob_attr *tb_n[__DM_SETS_A_NOTIF_MAX];
		char *path, *value, *notif_change;

		blobmsg_parse(dm_sets_attrib_policy, __DM_SETS_A_NOTIF_MAX,
				tb_n, blobmsg_data(cur), blobmsg_len(cur));

		path = NULL;
		value = NULL;
		notif_change = "0";

		if (tb_n[DM_SETS_A_NOTIF_PATH])
			path = blobmsg_get_string(tb_n[DM_SETS_A_NOTIF_PATH]);

		if (tb_n[DM_SETS_A_NOTIF_VALUE])
			value = blobmsg_get_string(tb_n[DM_SETS_A_NOTIF_VALUE]);

		if (tb_n[DM_SETS_A_NOTIF_CHANGE])
			notif_change = blobmsg_get_string(tb_n[DM_SETS_A_NOTIF_CHANGE]);

		if (path == NULL || value == NULL)
			continue;

		bbf_set_notification(&bb, path, value, notif_change);
	}

	blobmsg_close_array(&bb, a);
	ubus_send_reply(ctx, req, bb.head);
	return 0;
}

static int usp_set(struct ubus_context *ctx,
		__unused struct ubus_object *obj,
		struct ubus_request_data *req,
		__unused const char *method,
		struct blob_attr *msg)
{
	struct blob_attr *tb[__DM_SET_MAX];
	char *path = NULL;
	char *value = NULL;
	char *key = "";
	void *a;
	int fault;

	blobmsg_parse(dm_set_policy, __DM_SET_MAX, tb, blob_data(msg), blob_len(msg));

	if (tb[DM_SET_PATH])
		path = blobmsg_get_string(tb[DM_SET_PATH]);

	if (tb[DM_SET_VALUE])
		value = blobmsg_get_string(tb[DM_SET_VALUE]);

	if (path == NULL || value == NULL)
		return UBUS_STATUS_INVALID_ARGUMENT;

	if (tb[DM_SET_PARAMETER_KEY])
		key = blobmsg_get_string(tb[DM_SET_PARAMETER_KEY]);

	bbf_set_data_type(tb[DM_SET_PROTO]);

	blob_buf_init(&bb, 0);
	a = blobmsg_open_array(&bb, "parameters");
	fault = bbf_set_value(&bb, path, value, key);
	blobmsg_close_array(&bb, a);

	ubus_send_reply(ctx, req, bb.head);

	if (!fault)
		bbf_restart_services();

	return 0;
}

static void add_del_path_fixup(char *path, size_t maxlen)
{
	const size_t len = strlen(path);

	if (path[len - 1] != '.' && len < maxlen - 2) {
		path[len + 0] = '.';
		path[len + 1] = 0;
	}
}

static int bbfd_add_del_handler(struct ubus_context *ctx,
				__unused struct ubus_object *obj,
				struct ubus_request_data *req,
				const char *method,
				struct blob_attr *msg)
{
	struct blob_attr *tb[__DM_ADD_MAX];
	char path[MAXNAMLEN];
	const char *pkey = NULL;

	blobmsg_parse(dm_add_policy, __DM_ADD_MAX, tb, blob_data(msg), blob_len(msg));

	if ((tb[DM_ADD_PATH]))
		strncpyt(path, blobmsg_get_string(tb[DM_ADD_PATH]), sizeof(path));
	else
		return UBUS_STATUS_INVALID_ARGUMENT;

	if (tb[DM_ADD_PARAMETER_KEY])
		pkey = blobmsg_get_string(tb[DM_ADD_PARAMETER_KEY]);

	bbf_set_data_type(tb[DM_ADD_PROTO]);

	/* Path needs to have an tailing '.' on add/del */
	add_del_path_fixup(path, sizeof(path));

	blob_buf_init(&bb, 0);

	if (!strcmp(method, "add_object"))
		bbf_add_object(&bb, path, pkey);
	else
		bbf_del_object(&bb, path, pkey);

	ubus_send_reply(ctx, req, bb.head);
	return 0;
}

static struct bbfd_async_req * bbfd_async_req_new()
{
	struct bbfd_async_req *r = malloc(sizeof(*r));

	if (r) {
		memset(&r->process, 0, sizeof(r->process));
		r->data_fd = -1;
	}
	return r;
}

static void bbfd_async_req_free(struct bbfd_async_req *r)
{
	if (r->data_fd != -1)
		close(r->data_fd);

	free(r);
}

static void operate_complete_cb(struct uloop_process *p, __unused int ret)
{
	struct bbfd_async_req *r = container_of(p, struct bbfd_async_req, process);
	void *buf = calloc(1, OPERATE_RESPONSE_MAXLEN);

	if (buf) {
		read(r->data_fd, buf, OPERATE_RESPONSE_MAXLEN);
		ubus_send_reply(ctx, &r->req, buf);
		free(buf);
	}

	ubus_complete_deferred_request(ctx, &r->req, 0);
	bbfd_async_req_free(r);
}

static int bbfd_operate_main(struct blob_buf *b, struct blob_attr *msg)
{
	struct blob_attr *tb[__DM_OPERATE_MAX];
	char *input_args_json = NULL;
	const char *path;
	void *a;

	blobmsg_parse(dm_operate_policy, __DM_OPERATE_MAX, tb, blob_data(msg), blob_len(msg));

	if (!tb[DM_OPERATE_PATH])
		return UBUS_STATUS_INVALID_ARGUMENT;

	path = blobmsg_get_string(tb[DM_OPERATE_PATH]);

	if (tb[DM_OPERATE_PARAMS])
		input_args_json = blobmsg_format_json(tb[DM_OPERATE_PARAMS], true);

	bbf_set_data_type(tb[DM_ADD_PROTO]);

	blob_buf_init(b, 0);
	a = blobmsg_open_array(b, "parameters");
	bbf_operate(b, path, input_args_json);
	blobmsg_close_array(b, a);

	free(input_args_json);
	return 0;
}

static int bbfd_operate_start_deferred(struct ubus_context *ctx,
					struct ubus_request_data *req,
					struct blob_attr *msg)
{
	struct bbfd_async_req *r = bbfd_async_req_new();
	int data_pipe[2];
	pid_t child;

	if (r == NULL)
		return UBUS_STATUS_UNKNOWN_ERROR;

	if (pipe2(data_pipe, O_NONBLOCK) == -1) {
		err_errno("pipe failed");
		goto err_out;
	}

	child = fork();
	if (child == -1) {
		close(data_pipe[0]);
		close(data_pipe[1]);
		err_errno("fork");
		goto err_out;
	} else if (child == 0) {
		/* child
		 */

		/* free fd's and memory inherited from parent */
		uloop_end();
		ubus_free(ctx);
		bbfd_async_req_free(r);
		fclose(stdin);
		fclose(stdout);
		fclose(stderr);

		/* close read end */
		close(data_pipe[0]);

		bbfd_operate_main(&bb, msg);

		/* write result and exit */
		write(data_pipe[1], bb.head, blob_pad_len(bb.head));
		exit(EXIT_SUCCESS);
	}

	/* parent
	 */

	/* close write end */
	close(data_pipe[1]);

	r->data_fd = data_pipe[0];
	r->process.pid = child;
	r->process.cb = operate_complete_cb;
	uloop_process_add(&r->process);
	ubus_defer_request(ctx, req, &r->req);
	return 0;

err_out:
	bbfd_async_req_free(r);
	return UBUS_STATUS_UNKNOWN_ERROR;
}

static int bbfd_operate(struct ubus_context *ctx,
				__unused struct ubus_object *obj,
				struct ubus_request_data *req,
				__unused const char *method,
				struct blob_attr *msg)
{
	return bbfd_operate_start_deferred(ctx, req, msg);
}

static struct ubus_method bbfd_methods[] = {
	UBUS_METHOD("get_values", bbfd_get_values, dm_get_safe_policy),
	UBUS_METHOD("get_attributes", bbfd_get_attributes, dm_get_safe_policy),
	UBUS_METHOD("get_names", bbfd_get_names, dm_get_safe_policy),
	UBUS_METHOD("get_instances", bbfd_get_instances, dm_get_safe_policy),
	UBUS_METHOD_NOARG("get_operates", bbfd_get_operates),
	UBUS_METHOD_NOARG("get_schema", bbfd_get_schema),
	UBUS_METHOD("set_attributes", usp_set_attributes, dm_sets_policy),
	UBUS_METHOD("set", usp_set, dm_set_policy),
	UBUS_METHOD("add_object", bbfd_add_del_handler, dm_add_policy),
	UBUS_METHOD("del_object", bbfd_add_del_handler, dm_add_policy),
	UBUS_METHOD("operate", bbfd_operate, dm_operate_policy),
};

static struct ubus_object_type bbfd_type =
	UBUS_OBJECT_TYPE("usp", bbfd_methods);

static struct ubus_object bbfd_obj = {
	.name = OBJ_NAME,
	.type = &bbfd_type,
	.methods = bbfd_methods,
	.n_methods = ARRAY_SIZE(bbfd_methods),
};

static struct ubus_object bbfd_raw_obj = {
	.name = OBJ_NAME_RAW,
	.type = &bbfd_type,
	.methods = bbfd_methods,
	.n_methods = ARRAY_SIZE(bbfd_methods),
};

static int bbfd_ubus_init()
{
	int rc;

	ctx = ubus_connect(NULL);
	if (ctx == NULL) {
		err_errno("Failed to connect to ubus");
		return -1;
	}

	rc = ubus_add_object(ctx, &bbfd_obj);
	if (rc)
		goto err;

	rc = ubus_add_object(ctx, &bbfd_raw_obj);
	if (rc)
		goto err;

	ubus_add_uloop(ctx);
	return 0;

err:
	err_ubus("Failed to add object", rc);
	if (rc == UBUS_STATUS_INVALID_ARGUMENT)
		err("bbfd already running ?\n");
	return -1;
}

int main()
{
	int rc = 0;

	uloop_init();

	rc = bbfd_ubus_init();
	if (rc == -1)
		goto out;

	uloop_run();

out:
	uloop_done();
	ubus_free(ctx);
	blob_buf_free(&bb);
	return rc;
}
