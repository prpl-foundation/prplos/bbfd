#include "bbf.h"

#include <libbbfdm/dmentry.h>
#include <libbbfdm/dmbbfcommon.h>

struct print_format {
	char data:1;
	char type:1;
};


static void get_print_format(int cmd, struct print_format *fmt)
{
	switch (cmd) {
	case CMD_GET_VALUE:
	case CMD_GET_NOTIFICATION:
		fmt->data = 1;
		fmt->type = 0;
		break;
	case CMD_GET_INSTANCES:
		fmt->data = 0;
		fmt->type = 0;
		break;
	case CMD_USP_LIST_OPERATE:
		fmt->data = 1;
		fmt->type = 1;
		break;
	case CMD_GET_SCHEMA:
	case CMD_GET_NAME:
	default:
		fmt->data = 1;
		fmt->type = 1;
		break;
	}
}

static void bbf_init(struct dmctx *dm_ctx)
{
	dm_ctx_init(dm_ctx, INSTANCE_MODE_NUMBER);
}

static void bbf_cleanup(struct dmctx *dm_ctx)
{
	dm_ctx_clean(dm_ctx);
}

void bbf_set_data_type(struct blob_attr *proto)
{
	int type;

	if (proto) {
		const char *val = blobmsg_get_string(proto);

		if (!strcmp("cwmp", val))
			type = BBFDM_CWMP;
		else if (!strcmp("usp", val))
			type = BBFDM_USP;
		else
			type = BBFDM_BOTH;
	} else {
		type = BBFDM_BOTH;
	}

	set_bbfdatamodel_type(type);
}

static int bbf_exec(int operation, char *path, struct dmctx *dm_ctx, char *arg1, char *arg2)
{
	int fault = dm_entry_param_method(dm_ctx, operation, path, arg1, arg2);

	if (dm_ctx->list_fault_param.next != &dm_ctx->list_fault_param) {
		struct param_fault *p;
		list_for_each_entry(p, &dm_ctx->list_fault_param, list) {
			return p->fault;
		}
	}
	return fault;
}

static int bbf_get(int operation, char *path, struct dmctx *dm_ctx, char *next)
{
	return bbf_exec(operation, path, dm_ctx, next, NULL);
}

int bbf_get_raw(int cmd, char *path, struct blob_buf *bb, char *nxt_lvl)
{
	struct dmctx dm_ctx = {};
	struct dm_parameter *n;
	void *table;
	int fault;
	struct print_format fmt;

	get_print_format(cmd, &fmt);

	bbf_init(&dm_ctx);
	fault = bbf_get(cmd, path, &dm_ctx, nxt_lvl);
	if(!fault) {
		list_for_each_entry(n, &dm_ctx.list_parameter, list) {
			table = blobmsg_open_table(bb, NULL);

			blobmsg_add_string(bb, "parameter", n->name);

			if (fmt.data && n->data)
				blobmsg_add_string(bb, "value", n->data);

			if (fmt.type && n->type)
				blobmsg_add_string(bb, "type", n->type);

			blobmsg_close_table(bb, table);
		}
	} else {
		table = blobmsg_open_table(bb, NULL);
		blobmsg_add_string(bb, "path", path);
		blobmsg_add_u32(bb, "fault", (uint32_t)fault);
		blobmsg_close_table(bb, table);
	}
	bbf_cleanup(&dm_ctx);
	return fault;
}

int bbf_get_blob(int cmd, char *path, struct blob_buf *bb, char *nxt_lvl)
{
	struct dmctx dm_ctx = {};
	struct dm_parameter *n;
	size_t plen = strlen(path);
	int fault;

	bbf_init(&dm_ctx);
	fault = bbf_get(cmd, path, &dm_ctx, nxt_lvl);
	if(!fault) {
		void *t = NULL;
		size_t poff = 0;

		if (path[plen - 1] == '.') {
			t = blobmsg_open_table(bb, path);
			poff = plen;
		}

		list_for_each_entry(n, &dm_ctx.list_parameter, list)
			blobmsg_add_string(bb, n->name + poff,  n->data);

		if (t)
			blobmsg_close_table(bb, t);
	} else {
		blobmsg_add_string(bb, "path", path);
		blobmsg_add_u32(bb, "fault", (uint32_t)fault);
	}
	bbf_cleanup(&dm_ctx);
	return fault;
}

int bbf_set_notification(struct blob_buf *bb, char *path, char *notif, char *change)
{
	struct dmctx dm_ctx = {};
	void *t;
	int fault;

	bbf_init(&dm_ctx);
	fault = bbf_exec(CMD_SET_NOTIFICATION, path, &dm_ctx, notif, change);

	t = blobmsg_open_table(bb, NULL);
	blobmsg_add_string(bb, "path", path);
	if (fault)
		blobmsg_add_u32(bb, "fault", fault);
	blobmsg_close_table(bb, t);

	return fault;
}

int bbf_set_value(struct blob_buf *bb, char *path, char *value, char *key)
{
	struct dmctx dm_ctx = {};
	int fault;
	void *bb_array = blobmsg_open_table(bb, NULL);

	bbf_init(&dm_ctx);

	fault = bbf_exec(CMD_SET_VALUE, path, &dm_ctx, value, NULL);
	if (!fault) {
		fault = dm_entry_apply(&dm_ctx, CMD_SET_VALUE, key, NULL);

		if (dm_ctx.list_fault_param.next != &dm_ctx.list_fault_param) {
			struct param_fault *p;
			list_for_each_entry(p, &dm_ctx.list_fault_param, list) {
				fault = p->fault;
				break;
			}
		}
	}

	if (fault) {
		blobmsg_add_u8(bb, "status", false);
		blobmsg_add_string(bb, "path", path);
		blobmsg_add_u32(bb, "fault", (uint32_t)fault);
	} else {
		blobmsg_add_u8(bb, "status", true);
		blobmsg_add_string(bb, "path", path);
	}

	blobmsg_close_table(bb, bb_array);
	bbf_cleanup(&dm_ctx);
	return fault;
}

void bbf_add_object(struct blob_buf *bb, char *path, const char *pkey)
{
	struct dmctx dm_ctx = {};
	uint32_t fault;

	bbf_init(&dm_ctx);

	if (pkey == NULL || pkey[0] == 0)
		pkey = "true";

	fault = dm_entry_param_method(&dm_ctx, CMD_ADD_OBJECT, path, (char *)pkey, NULL);
	if (fault) {
		blobmsg_add_u32(bb, "fault", fault);
	} else {
		if (dm_ctx.addobj_instance) {
			blobmsg_add_u8(bb, "status", 1);
			blobmsg_add_string(bb, "instance", dm_ctx.addobj_instance);
		} else {
			blobmsg_add_u8(bb, "status", 0);
			blobmsg_add_u32(bb, "fault", FAULT_9002);
		}

		dm_entry_restart_services();
	}

	bbf_cleanup(&dm_ctx);
}

void bbf_del_object(struct blob_buf *bb, char *path, const char *pkey)
{
	struct dmctx dm_ctx = {};
	uint32_t fault;

	bbf_init(&dm_ctx);

	if (pkey == NULL || pkey[0] == 0)
		pkey = "true";

	fault = dm_entry_param_method(&dm_ctx, CMD_DEL_OBJECT, path, (char *)pkey, NULL);

	blobmsg_add_string(bb, "parameter", path);
	if (fault) {
		blobmsg_add_u8(bb, "status", 0);
		blobmsg_add_u32(bb, "fault", fault);
	} else {
		blobmsg_add_u8(bb, "status", 1);
		dm_entry_restart_services();
	}
	bbf_cleanup(&dm_ctx);
}

void bbf_restart_services()
{
	dm_entry_restart_services();
}

static void add_operate_args(struct blob_buf *bb, const operation_args *args)
{
	void *t = blobmsg_open_table(bb, "args");
	void *a;
	unsigned i;

	if (args->in) {
		a = blobmsg_open_array(bb, "in");
		for (i = 0; args->in[i]; i++)
			blobmsg_add_string(bb, NULL, args->in[i]);
		blobmsg_close_array(bb, a);
	}

	if (args->out) {
		a = blobmsg_open_array(bb, "out");
		for (i = 0; args->out[i]; i++)
			blobmsg_add_string(bb, NULL, args->out[i]);
		blobmsg_close_array(bb, a);
	}

	blobmsg_close_table(bb, t);
}

void bbf_get_raw_usp_noarg(struct blob_buf *bb, int cmd)
{
	struct dmctx dm_ctx = {};
	struct dm_parameter *n;
	struct print_format fmt;
	int fault;

	set_bbfdatamodel_type(BBFDM_USP);
	bbf_init(&dm_ctx);
	fault = dm_entry_param_method(&dm_ctx, cmd, NULL, NULL, NULL);
	if (fault)
		return;

	get_print_format(cmd, &fmt);

	list_for_each_entry(n, &dm_ctx.list_parameter, list) {
		void *t = blobmsg_open_table(bb, NULL);

		blobmsg_add_string(bb, "parameter", n->name);

		if (n->data && fmt.data) {
			if (cmd == CMD_USP_LIST_OPERATE)
				add_operate_args(bb, (const operation_args *)n->data);
			else
				blobmsg_add_string(bb, "writeable", n->data[0] ? "1" : "0");
		}

		if (n->type)
			blobmsg_add_string(bb, "type", n->type);

		blobmsg_close_table(bb, t);
	}
	bbf_cleanup(&dm_ctx);
}

void bbf_operate(struct blob_buf *bb, const char *path, char *input_args)
{
	struct dmctx dm_ctx = {};
	struct dm_parameter *n;
	int fault;
	void *t;

	bbf_init(&dm_ctx);
	fault = dm_entry_param_method(&dm_ctx, CMD_USP_OPERATE,
					(char *)path, input_args, NULL);
	if (fault != SUCCESS) {
		t = blobmsg_open_table(bb, NULL);
		blobmsg_add_u32(bb, "fault", fault);
		blobmsg_close_table(bb, t);
	} else {
		list_for_each_entry(n, &dm_ctx.list_parameter, list) {
			t = blobmsg_open_table(bb, NULL);
			blobmsg_add_string(bb, "parameter", n->name);
			blobmsg_add_string(bb, "value", n->data);
			blobmsg_close_table(bb, t);
		}
	}

	bbf_cleanup(&dm_ctx);
}
