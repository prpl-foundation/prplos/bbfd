#ifndef BBFD_H
#define BBFD_H

#include <stdio.h>
#include <errno.h>
#include <string.h>

#ifndef __unused
#define __unused __attribute__((unused))
#endif

#define err(fmt, args...) \
	fprintf(stderr, "%s: " fmt, __FUNCTION__, ##args)

#define nfo(fmt, args...) \
	printf("%s: " fmt, __FUNCTION__, ##args)

#define err_errno(fmt, args...) \
	fprintf(stderr, "%s: " fmt ": %s\n", \
		__FUNCTION__, ##args, strerror(errno))

#endif
