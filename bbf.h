#ifndef BBFD_BBF_H
#define BBFD_BBF_H

#include <libbbf_api/dmbbf.h>
#include <libubox/blobmsg.h>

void bbf_set_data_type(struct blob_attr *proto);
int bbf_get_raw(int cmd, char *path, struct blob_buf *bb, char *nxt_lvl);
int bbf_get_blob(int cmd, char *path, struct blob_buf *bb, char *nxt_lvl);
int bbf_set_notification(struct blob_buf *bb, char *path, char *notif, char *change);
int bbf_set_value(struct blob_buf *bb, char *path, char *value, char *key);
void bbf_del_object(struct blob_buf *bb, char *path, const char *pkey);
void bbf_add_object(struct blob_buf *bb, char *path, const char *pkey);
void bbf_restart_services();
void bbf_get_raw_usp_noarg(struct blob_buf *bb, int cmd);
void bbf_operate(struct blob_buf *bb, const char *path, char *input_args);

#endif
