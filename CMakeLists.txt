cmake_minimum_required(VERSION 2.6)
project(bbfd C)
add_definitions(--std=gnu99 -g3 -Os -Wall -Wextra -D_GNU_SOURCE)

add_executable(bbfd main.c bbf.c)
target_link_libraries(bbfd ubus ubox bbfdm blobmsg_json)

install(TARGETS bbfd RUNTIME DESTINATION bin)
